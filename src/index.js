import React from "react";
import ReactDOM from "react-dom";
import { cache } from "./cache";

import App from "./App";
import { ApolloClient, ApolloProvider } from "@apollo/client";

const client = new ApolloClient({
  cache,
  uri: "https://tmdb.sandbox.zoosh.ie/dev/graphql",
  resolvers: {}
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById("root")
);