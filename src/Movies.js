import React from "react";
import { useLazyQuery } from "@apollo/client";
import gql from "graphql-tag";
import './Movies.css'; 

/*
const SEARCH_MOVIES = gql` 
  query ($searchQuery: String) {
    searchMovies(filter: {
      name: {
        contains: $searchQuery
      }
    })
      {
      id
      name
      overview
      releaseDate
      cast {
        id
        person {
          name
        }
        role {
          ... on Cast {
            character
          }
        }
      }
    }
  }
`
*/

const GET_MOVIES = gql` 
    query fetchPopular {
        movies: popularMovies {
          id
          name
          overview
          releaseDate
          img: poster {
            url: custom(size: "w185_and_h278_bestv2")
          }
          reviews {
            id
            author
            content
            language {
              code
              name
            }
          }
        }
    }
`

export function Movies() {
  const [getMovies, { loading, data }] = useLazyQuery(GET_MOVIES);
  if (loading) return <div class="loader"></div>

  if (data && data.movies) {
    console.log(data.movies);
  }

/*
  const Search = () => {
    const [searchFilter, setSearchFilter] = useState('');
    const [executeSearch, { data }] = useLazyQuery(
        SEARCH_MOVIES
      );
    return (
      <>
        <div>
          Search
          <input
            type="text"
            onChange={(e) => setSearchFilter(e.target.value)}
          />
          <button
          onClick={() =>
            executeSearch({
              variables: { filter: searchFilter }
            })
          }
        >
          OK
        </button>
      </div>
      {data &&
        data.searchMovies.links.map((link, index) => (
          <Link key={link.id} link={link} index={index} />
        ))}
    </>
  );
};

export default Search;
*/

  return (
    /*
    <div>
        <form>
       <input
         placeholder="Search for..."
         ref={input => this.search = input}
         onChange={this.handleInputChange}
       />
     </form>
     */
    
    <div>
      <input type="text" placeholder="Search.." name="search"></input>
      <button onClick={() => getMovies()}>
        Get all movies!
      </button>
      {data &&
        data.movies &&
        data.movies.map((c, i) => <div key={i}> 
        <img src={`${c.img.url}`} alt={`${c.name}`} width="250" height="300"></img>
        <br></br>
        <a href={`https://en.wikipedia.org/w/api.php?origin=*&action=opensearch&search=${c.name}`}>{c.name}</a>
        <p>{c.overview}</p>
        </div>)}
    </div>
  );
  
}
